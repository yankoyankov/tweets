# Tweets

Scrapping tweets and make simple stats.

### Prerequisites

You need to have:

```
Docker version 19.03.4
Docker-compose version 1.21.2
Git
```

### Installing

Steps for starting development server:

```
git clone https://yankoyankov@bitbucket.org/yankoyankov/tweets.git
cd tweets
docker-compose up -d
docker-compose exec api bash
go run main.go
```

## Running the tests

```
go test ./...
```