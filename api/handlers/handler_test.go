package handlers

import (
	"net/http"
	"testing"
	"go.mongodb.org/mongo-driver/bson"
	"api/models"
	"net/http/httptest"
)

type mock struct{}

func (db mock) ParseTweets(year int, ch chan<- models.TweetsResult) {}

func (db mock) InsertTweets(tweets []models.Tweet) {}

func (db mock) DropTweets() {}

func (db mock) GetSources() []bson.M {
	return []bson.M{bson.M{"_id":"Twitter for iPhone","count":13572}}
}

func (db mock) GetPerYear() []bson.M {
	return []bson.M{bson.M{"_id":2015,"count":7536}}
}

func (db mock) GetPerHours() []bson.M {
	return []bson.M{bson.M{"_id":0,"count":1188}}
}


func TestGetTweetsSources(t *testing.T) {
	req, _ := http.NewRequest("GET", "/get-tweets-source", nil)

	rr := httptest.NewRecorder()

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		GetTweetsSources(w, r, mock{})
	})

	testHandler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `[{"_id":"Twitter for iPhone","count":13572}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestGetTweetsPerYear(t *testing.T) {
	req, _ := http.NewRequest("GET", "/get-tweets-per-year", nil)

	rr := httptest.NewRecorder()

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		GetTweetsPerYear(w, r, mock{})
	})

	testHandler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `[{"_id":2015,"count":7536}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}


func TestGetTweetsPerHours(t *testing.T) {
	req, _ := http.NewRequest("GET", "/get-tweets-per-hours", nil)

	rr := httptest.NewRecorder()

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		GetTweetsPerHours(w, r, mock{})
	})

	testHandler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `[{"_id":0,"count":1188}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}