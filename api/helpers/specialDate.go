package helpers

import (
	"time"
	"strings"
)

type SpecialDate struct {
	time.Time
}

func (sd *SpecialDate) UnmarshalJSON(input []byte) error {
	strInput := string(input)
	strInput = strings.Trim(strInput, `"`)
	newTime, err := time.Parse(time.RubyDate, strInput)
	if err != nil {
		return err
	}
	sd.Time = newTime
	return nil
}