package main

import (
	"net/http"
	"api/models"
	"api/handlers"
	"api/repos"
	"context"
	"fmt"
)

const fromYear = 2014
const toYear = 2019

func main() {
	//Init DB Connection
	mongo := repos.Mongodb{}
	mongo.InitMongo()
	defer mongo.Client.Disconnect(context.TODO())
	twModel := models.TweetCollection{DBCollection: mongo.GetTweetColection()}

	//Initial database population
	scrapeTweets(twModel)

	//Init listeners
	http.HandleFunc("/get-tweets-source", func(w http.ResponseWriter, r *http.Request) {
		handlers.GetTweetsSources(w, r, twModel)
	})
	http.HandleFunc("/get-tweets-per-year", func(w http.ResponseWriter, r *http.Request) {
		handlers.GetTweetsPerYear(w, r, twModel)
	})
	http.HandleFunc("/get-tweets-per-hours", func(w http.ResponseWriter, r *http.Request) {
		handlers.GetTweetsPerHours(w, r, twModel)
	})
	http.ListenAndServe(":8080", nil)
}

/*
 * Asynchronously make request to the endpoint for scraping tweets
 * Insert the result into DB
 */
func scrapeTweets(tweet models.Datastore)  {
	//Drop everything from previous app inits
	tweet.DropTweets()

	//Make asynchronous requests for each year
	ch := make(chan models.TweetsResult, toYear - fromYear)
	for y:=fromYear; y<=toYear; y++ {
		go tweet.ParseTweets(y, ch)
	}

	//Receive asynchronous responses
	for y:=fromYear; y<=toYear; y++ {
		tw := <- ch
		if tw.Error != nil{
			fmt.Errorf(tw.Error.Error())
			continue
		}
		tweet.InsertTweets(tw.Tweets)
		fmt.Println("Tweets parsed and inserted per year:", y, len(tw.Tweets))
	}
	close(ch)
}
