package models

import (
	"api/helpers"
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"io/ioutil"
	"net/http"
	"strconv"
)

type Tweet struct {
	Source             string              `json:"source" bson:"source",`
	IDStr              string              `json:"id_str" bson:"id_str",`
	Text               string              `json:"text" bson:"text",`
	CreatedAt          helpers.SpecialDate `json:"created_at" bson:"created_at",`
	RetweetCount       int                 `json:"retweet_count" bson:"retweet_count",`
	InReplyToUserIDStr interface{}         `json:"in_reply_to_user_id_str" bson:"in_reply_to_user_id_str",`
	FavoriteCount      int                 `json:"favorite_count" bson:"favorite_count",`
	IsRetweet          bool                `json:"is_retweet" bson:"is_retweet",`
}

type TweetsResult struct {
	Tweets []Tweet
	Error  error
}

type TweetCollection struct {
	DBCollection *mongo.Collection
}

type Datastore interface {
	ParseTweets(year int, ch chan<- TweetsResult)
	InsertTweets(tweets []Tweet)
	GetSources() []bson.M
	GetPerYear() []bson.M
	GetPerHours() []bson.M
	DropTweets()
}

func (db TweetCollection) ParseTweets(year int, ch chan<- TweetsResult) {

	url := fmt.Sprintf("%s/data/realdonaldtrump/%s.json", helpers.TweetURL, strconv.Itoa(year))

	var tweets []Tweet
	var TweetsResult TweetsResult

	resp, err := http.Get(url)
	defer resp.Body.Close()
	if err != nil {
		TweetsResult.Error = err
		ch <- TweetsResult
	}

	// read json http response
	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		TweetsResult.Error = err
		ch <- TweetsResult
	}

	err = json.Unmarshal([]byte(jsonDataFromHttp), &tweets)

	if err != nil {
		TweetsResult.Error = err
		ch <- TweetsResult
	}

	TweetsResult.Tweets = tweets

	ch <- TweetsResult
}

func (db TweetCollection) InsertTweets(tweets []Tweet) {
	for i := range tweets {
		//db. tweets[i].CreatedAt
		_, err := db.DBCollection.InsertOne(context.TODO(), tweets[i])

		if err != nil {
			fmt.Println("InsertOne ERROR:", err)
		}
	}
}

func (db TweetCollection) DropTweets() {
	err := db.DBCollection.Drop(context.TODO())
	if err != nil {
		fmt.Println("Drop ERROR:", err)
	}
}

func (db TweetCollection) GetSources() []bson.M {

	pipeline := []bson.M{bson.M{"$group": bson.M{"_id": "$source", "count": bson.M{"$sum": 1}}}, bson.M{"$sort": bson.M{"count": -1}}}

	cursor, err := db.DBCollection.Aggregate(
		context.Background(),
		pipeline,
	)
	if err != nil {
		fmt.Errorf(err.Error())
	}

	var result []bson.M
	cursor.All(context.Background(), &result)

	return result
}

func (db TweetCollection) GetPerYear() []bson.M {
	pipeline := []bson.M{bson.M{ "$group": bson.M{ "_id": bson.M{"$year": "$created_at.time"}, "count": bson.M{ "$sum": 1}}}, bson.M{ "$sort": bson.M{ "_id": 1}}}
	cursor, err := db.DBCollection.Aggregate(
		context.TODO(),
		pipeline,
	)
	if err != nil {
		fmt.Errorf(err.Error())
	}


	result := []bson.M{}
	err = cursor.All(context.TODO(), &result)
	if err != nil {
		fmt.Errorf(err.Error())
	}
	return result
}

func (db TweetCollection) GetPerHours() []bson.M {
	pipeline := []bson.M{bson.M{ "$group": bson.M{ "_id": bson.M{"$hour": "$created_at.time"}, "count": bson.M{ "$sum": 1}}}, bson.M{ "$sort": bson.M{ "_id": 1}}}
	cursor, err := db.DBCollection.Aggregate(
		context.TODO(),
		pipeline,
	)
	if err != nil {
		fmt.Errorf(err.Error())
	}


	result := []bson.M{}
	err = cursor.All(context.TODO(), &result)
	if err != nil {
		fmt.Errorf(err.Error())
	}
	return result
}
