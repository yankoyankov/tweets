package repos

import (
	"fmt"
	"log"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"api/helpers"
)

type Mongodb struct {
	Client *mongo.Client
}

func (c *Mongodb)InitMongo(){
	clientOptions := options.Client().ApplyURI(helpers.MongoDB)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")
	c.Client = client
}

func (c *Mongodb)GetTweetColection() *mongo.Collection {
	collection := c.Client.Database("test").Collection("trainers")
	return collection
}