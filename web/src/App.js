import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { render } from 'react-dom'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import StockChart from './components/Stock.js'
import Chart from './components/Chart.js'

import axios from 'axios';

function createSeries(json) {
    var series = [];
    json.forEach(function(serie) {
        series.push(
            {
                name: serie["_id"],
                y: serie["count"],
                color: '#FF00FF',
            }
        );
    });
    return {data: series};
}

class SourcesComponent extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            result: null
        }
    }

    componentDidMount(){
        const endpoint = 'http://165.22.91.151:8181/get-tweets-source';
        const setState = this.setState.bind(this);
        fetch(endpoint, {
            method: 'POST',
        })
        .then((resp) => resp.json())
            .then(function(response) {
                setState({result:  createSeries(response)});
        });
    }

    render(){
        return (<div>
                {this.state.result === null ?
                    <div>Loading...</div>
                :
                    <div>
                        {console.log("this.state.result")}
                        {console.log(this.state.result)}

                        <HighchartsReact
                            highcharts={Highcharts}
                            options={{
                                chart: {
                                    type: 'column'
                                },
                                series: [
                                    this.state.result
                                ],
                                title: {
                                    text: 'The most used sources'
                                },
                            }}/>
                    </div>
                }
            </div>
        );
    }
}

class PerYearComponent extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            result: null
        }
    }

    componentDidMount(){
        const endpoint = 'http://165.22.91.151:8181/get-tweets-per-year';
        const setState = this.setState.bind(this);
        fetch(endpoint, {
            method: 'POST',
        })
        .then((resp) => resp.json())
            .then(function(response) {
                setState({result:  createSeries(response)});
        });
    }

    render(){
        return (<div>
                {this.state.result === null ?
                    <div>Loading...</div>
                :
                    <div>
                        {console.log("this.state.result")}
                        {console.log(this.state.result)}

                        <HighchartsReact
                            highcharts={Highcharts}
                            options={{
                                chart: {
                                    type: 'column'
                                },
                                series: [
                                    this.state.result
                                ],
                                title: {
                                    text: 'Tweets per year'
                                },
                            }}/>
                    </div>
                }
            </div>
        );
    }
}

class PerYearHours extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            result: null
        }
    }

    componentDidMount(){
        const endpoint = 'http://165.22.91.151:8181/get-tweets-per-hours';
        const setState = this.setState.bind(this);
        fetch(endpoint, {
            method: 'POST',
        })
        .then((resp) => resp.json())
            .then(function(response) {
                setState({result:  createSeries(response)});
        });
    }

    render(){
        return (<div>
                {this.state.result === null ?
                    <div>Loading...</div>
                :
                    <div>
                        {console.log("this.state.result")}
                        {console.log(this.state.result)}

                        <HighchartsReact
                            highcharts={Highcharts}
                            options={{
                                chart: {
                                    type: 'column'
                                },
                                series: [
                                    this.state.result
                                ],
                                title: {
                                    text: 'Tweets by time of day(per hours)'
                                },
                            }}/>
                    </div>
                }
            </div>
        );
    }
}


function App() {
  return (
    <div className="App">
        <SourcesComponent subreddit="reactjs"/>
        <PerYearComponent subreddit="reactjs"/>
        <PerYearHours subreddit="reactjs"/>
    </div>
  );
}

export default App;